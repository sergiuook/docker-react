Dockerrun.aws.json
{
  "AWSEBDockerrunVersion" : 2,
  "containerDefinitions" : [
    {
      "name" : "client",
      "image" : "sergiustavila/complex-client",
      "hostname" : "client",
      "essential" : false,
      "memory": 128
    },
    {
      "name" : "server",
      "image" : "sergiustavila/complex-server",
      "hostname" : "api",
      "essential" : false,
      "memory": 128
    },
    {
      "name" : "worker",
      "image" : "sergiustavila/complex-worker",
      "hostname" : "worker",
      "essential" : false,
      "memory": 128
    },
    {
      "name" : "nginx",
      "image" : "sergiustavila/complex-nginx",
      "hostname" : "nginx",
      "essential" : true,
      "portMappings" : [
        {
          "hostPort" : 80,
          "containerPort" : 80
        }
      ],
      "links" : ["client", "server"],
      "memory": 128
    }
  ]
}